FROM ubuntu:20.04

MAINTAINER Julien Morot

RUN apt-get update && apt-get -y dist-upgrade && apt-get -y --purge autoremove && apt-get -y install postfix && apt-get -y clean

CMD ["/usr/sbin/postfix", "start-fg"]

EXPOSE 25


